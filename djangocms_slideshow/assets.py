from django_assets import Bundle, register


# --------------------------------- CSS ---------------------------------------

# djangocms_slideshow_less_css = Bundle(
#     'djangocms_slideshow/less/djangocms_slideshow.less',
#     depends='djangocms_slideshow/less/*.less',
#     filters='less',
#     output='djangocms_slideshow/gen/style_djangocms_slideshow.%(version)s.css'
# )

djangocms_slideshow_css = Bundle(
    # djangocms_slideshow_less_css,
    'djangocms_slideshow/bannerscollection_zoominout/bannerscollection_zoominout.css',
    'djangocms_slideshow/bannerscollection_zoominout/text_classes.css',
    'djangocms_slideshow/bannerscollection_zoominout/css3animations.css',
    filters='cssmin',
    output='djangocms_slideshow/gen/djangocms_slideshow.%(version)s.css'
)
# register('djangocms_slideshow_css', djangocms_slideshow_css)


# ---------------------------------- JS ---------------------------------------

uncompressed_js = [
    'djangocms_slideshow/bannerscollection_zoominout/bannerscollection_zoominout.js',
]
djangocms_slideshow_js = Bundle(
    'djangocms_slideshow/jqueryui-1.11.4/jquery-ui.js',
    'djangocms_slideshow/bannerscollection_zoominout/jquery.touchSwipe.min.js',
    *uncompressed_js,
    filters='jsmin',  # dummy minify filter
    output='djangocms_slideshow/gen/no-out.min.js'
)

# this is the solution to avoid minifying already minified 3rd party lib
# djangocms_slideshow_js_minify = Bundle(*uncompressed_js, filters='jsmin')
# djangocms_slideshow_js_min = Bundle(
#     'djangocms_slideshow/jqueryui-1.11.4/jquery-ui.min.js',
#     'djangocms_slideshow/bannerscollection_zoominout/jquery.touchSwipe.min.js',
#     'djangocms_slideshow/bannerscollection_zoominout/bannerscollection_zoominout.min.js',
#     djangocms_slideshow_js_minify,
#     output='djangocms_slideshow/gen/djangocms_slideshow.min.%(version)s.js'
# )

# from django.conf import settings
# if settings.DEBUG:
#     register('djangocms_slideshow_js', djangocms_slideshow_js)
# else:
#     register('djangocms_slideshow_js', djangocms_slideshow_js_min)


# ----------------------------- MOBILE CSS ------------------------------------

# mobile_less_css = Bundle(
#     'djangocms_slideshow/less/mobile_bsp.less',
#     depends='djangocms_slideshow/less/*.less',
#     filters='less',
#     output='djangocms_slideshow/gen/style_mobile.%(version)s.css'
# )

# mobile_css = Bundle(
#     'djangocms_slideshow/css/fonts.css',
#     'djangocms_slideshow/jquery_mobile_bsp.css',
#     'djangocms_slideshow/jquery.mobile.structure-1.2.0.min.css',
#     mobile_less_css,
#     'djangocms_slideshow/photoswipe.css',
#     depends='djangocms_slideshow/css/*.less',
#     filters='cssmin',
#     output='djangocms_slideshow/gen/mobile.%(version)s.css'
# )
#
# register('mobile_css', mobile_css)
