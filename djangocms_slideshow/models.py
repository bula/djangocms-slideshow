# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from cms.models import CMSPlugin
from filer.fields.image import FilerImageField


EASING_CHOICES = [
    ("linear", _("linear")),
    ("swing", _("swing")),
    ("ease", _("ease")),
    ("ease-in", _("ease-in")),
    ("ease-out", _("ease-out")),
    ("ease-in-out", _("ease-in-out")),
    ("easeInQuad", _("easeInQuad")),
    ("easeOutQuad", _("easeOutQuad")),
    ("easeInOutQuad", _("easeInOutQuad")),
    ("easeInCubic", _("easeInCubic")),
    ("easeOutCubic", _("easeOutCubic")),
    ("easeInOutCubic", _("easeInOutCubic")),
    ("easeInQuart", _("easeInQuart")),
    ("easeOutQuart", _("easeOutQuart")),
    ("easeInOutQuart", _("easeInOutQuart")),
    ("easeInSine", _("easeInSine")),
    ("easeOutSine", _("easeOutSine")),
    ("easeInOutSine", _("easeInOutSine")),
    ("easeInExpo", _("easeInExpo")),
    ("easeOutExpo", _("easeOutExpo")),
    ("easeInOutExpo", _("easeInOutExpo")),
    ("easeInQuint", _("easeInQuint")),
    ("easeOutQuint", _("easeOutQuint")),
    ("easeInOutQuint", _("easeInOutQuint")),
    ("easeInCirc", _("easeInCirc")),
    ("easeOutCirc", _("easeOutCirc")),
    ("easeInOutCirc", _("easeInOutCirc")),
    ("easeInBack", _("easeInBack")),
    ("easeOutBack", _("easeOutBack")),
    ("easeInOutBack", _("easeInOutBack")),
]

CSS3_ANIMATION_CHOICES = [
    ("alreadyRotate", _("alreadyRotate")),
    ("alreadyRotate_90", _("alreadyRotate_90")),
    ("awesome", _("awesome")),
    ("awesome ul", _("awesome ul")),
    ("awesome ul li", _("awesome ul li")),
    ("bigEntrance", _("bigEntrance")),
    ("boingInUp", _("boingInUp")),
    ("boingOutDown", _("boingOutDown")),
    ("bombLeftOut", _("bombLeftOut")),
    ("bombRightOut", _("bombRightOut")),
    ("bounce", _("bounce")),
    ("bounceIn", _("bounceIn")),
    ("bounceOut", _("bounceOut")),
    ("changeColorToRed", _("changeColorToRed")),
    ("changeColorToBlack", _("changeColorToBlack")),
    ("changeColorToWhite", _("changeColorToWhite")),
    ("changeColorToOrange", _("changeColorToOrange")),
    ("changeColorToPink", _("changeColorToPink")),
    ("CoolBaloonFixed", _("CoolBaloonFixed")),
    ("CoolBaloonFullWidth", _("CoolBaloonFullWidth")),
    ("CoolCloud1", _("CoolCloud1")),
    ("CoolCloud2", _("CoolCloud2")),
    ("CoolCloud3", _("CoolCloud3")),
    ("coolBarBottom", _("coolBarBottom")),
    ("coolBarCenter", _("coolBarCenter")),
    ("coolBarCenterFixed", _("coolBarCenterFixed")),
    ("coolBarRight", _("coolBarRight")),
    ("coolBarLeft", _("coolBarLeft")),
    ("coolBarRightMin", _("coolBarRightMin")),
    ("coolBarLeftMin", _("coolBarLeftMin")),
    ("coolBarRightMinRegular", _("coolBarRightMinRegular")),
    ("coolBarLeftMinRegular", _("coolBarLeftMinRegular")),
    ("dance", _("dance")),
    ("expandUp", _("expandUp")),
    ("fadeIn", _("fadeIn")),
    ("flash", _("flash")),
    ("flashBang", _("flashBang")),
    ("flip", _("flip")),
    ("flipBookCenter_Cover1", _("flipBookCenter_Cover1")),
    ("flipBookCenter_Cover2", _("flipBookCenter_Cover2")),
    ("flipBookLeft_Cover1", _("flipBookLeft_Cover1")),
    ("flipBookLeft_Cover2", _("flipBookLeft_Cover2")),
    ("flipBookRight_Cover3", _("flipBookRight_Cover3")),
    ("flipBookRight_Cover4", _("flipBookRight_Cover4")),
    ("flipInX", _("flipInX")),
    ("flipInY", _("flipInY")),
    ("flipOutX", _("flipOutX")),
    ("flipOutY", _("flipOutY")),
    ("flipXFast3d", _("flipXFast3d")),
    ("flipX3dPerpetuum", _("flipX3dPerpetuum")),
    ("flipY3dPerpetuum", _("flipY3dPerpetuum")),
    ("flipYFastRight3d", _("flipYFastRight3d")),
    ("flipYFastLeft3d", _("flipYFastLeft3d")),
    ("flipYFastRight3dChangeColor", _("flipYFastRight3dChangeColor")),
    ("flipYFastLeft3dChangeColor", _("flipYFastLeft3dChangeColor")),
    ("flipYFastLeft3dSpecialColor", _("flipYFastLeft3dSpecialColor")),
    ("flipYFastLeft3dSpecialColor2", _("flipYFastLeft3dSpecialColor2")),
    ("floating", _("floating")),
    ("floatingEasy", _("floatingEasy")),
    ("foolishIn", _("foolishIn")),
    ("foolishOut", _("foolishOut")),
    ("hatch", _("hatch")),
    ("hinge", _("hinge")),
    ("hinge2", _("hinge2")),
    ("holeOut", _("holeOut")),
    ("jamp", _("jamp")),
    ("jello", _("jello")),
    ("journal", _("journal")),
    ("lightSpeedInLeft", _("lightSpeedInLeft")),
    ("lightSpeedInRight", _("lightSpeedInRight")),
    ("lightSpeedOutRight", _("lightSpeedOutRight")),
    ("lightSpeedOutLeft", _("lightSpeedOutLeft")),
    ("magic", _("magic")),
    ("magnifying", _("magnifying")),
    ("openDownRight", _("openDownRight")),
    ("openUpLeft", _("openUpLeft")),
    ("openUpRight", _("openUpRight")),
    ("openDownLeftRetourn", _("openDownLeftRetourn")),
    ("openDownRightRetourn", _("openDownRightRetourn")),
    ("openUpLeftRetourn", _("openUpLeftRetourn")),
    ("openUpRightRetourn", _("openUpRightRetourn")),
    ("openDownLeftOut", _("openDownLeftOut")),
    ("openDownRightOut", _("openDownRightOut")),
    ("openUpLeftOut", _("openUpLeftOut")),
    ("openUpRightOut", _("openUpRightOut")),
    ("perpetuumChainLink1", _("perpetuumChainLink1")),
    ("perpetuumChainLink2", _("perpetuumChainLink2")),
    ("perspectiveLeft", _("perspectiveLeft")),
    ("perspectiveRight", _("perspectiveRight")),
    ("perspectiveRightIn", _("perspectiveRightIn")),
    ("perspectiveUp", _("perspectiveUp")),
    ("perspectiveDownZero", _("perspectiveDownZero")),
    ("perspectiveDown", _("perspectiveDown")),
    ("perspectiveLeftRetourn", _("perspectiveLeftRetourn")),
    ("perspectiveRightRetourn", _("perspectiveRightRetourn")),
    ("perspectiveUpRetourn", _("perspectiveUpRetourn")),
    ("perspectiveDownRetourn", _("perspectiveDownRetourn")),
    ("rollInRight", _("rollInRight")),
    ("rollInLeft", _("rollInLeft")),
    ("rollOutRight", _("rollOutRight")),
    ("rollOutLeft", _("rollOutLeft")),
    ("rotateEaseForward", _("rotateEaseForward")),
    ("rotateEaseBackward", _("rotateEaseBackward")),
    ("rotateFastForward", _("rotateFastForward")),
    ("rotateFastBackward", _("rotateFastBackward")),
    ("rotateLeft", _("rotateLeft")),
    ("rotateRight", _("rotateRight")),
    ("rotateUp", _("rotateUp")),
    ("rotateDown", _("rotateDown")),
    ("rotateInRight", _("rotateInRight")),
    ("rotateInLeft", _("rotateInLeft")),
    ("rotateInDownLeft", _("rotateInDownLeft")),
    ("rotateInDownRight", _("rotateInDownRight")),
    ("rotateInUpLeft", _("rotateInUpLeft")),
    ("rotateInUpRight", _("rotateInUpRight")),
    ("rotateOut", _("rotateOut")),
    ("rotateOutDownLeft", _("rotateOutDownLeft")),
    ("rotateOutDownRight", _("rotateOutDownRight")),
    ("rotateOutUpLeft", _("rotateOutUpLeft")),
    ("rotateOutUpRight", _("rotateOutUpRight")),
    ("rubberBand", _("rubberBand")),
    ("scaleBounce", _("scaleBounce")),
    ("scaleIn", _("scaleIn")),
    ("scaleInMin", _("scaleInMin")),
    ("scaleInExit", _("scaleInExit")),
    ("scaleInExitBounce", _("scaleInExitBounce")),
    ("scaleOut", _("scaleOut")),
    ("scaleOutBounce", _("scaleOutBounce")),
    ("shake", _("shake")),
    ("skew", _("skew")),
    ("slideDown", _("slideDown")),
    ("slideUp", _("slideUp")),
    ("slideLeft", _("slideLeft")),
    ("slideRight", _("slideRight")),
    ("slideExpandUp", _("slideExpandUp")),
    ("snow1", _("snow1")),
    ("snow2", _("snow2")),
    ("snow3", _("snow3")),
    ("spaceInUp", _("spaceInUp")),
    ("spaceInDown", _("spaceInDown")),
    ("spaceInLeft", _("spaceInLeft")),
    ("spaceInRight", _("spaceInRight")),
    ("spaceOutUp", _("spaceOutUp")),
    ("stretchRight", _("stretchRight")),
    ("swap", _("swap")),
    ("swashIn", _("swashIn")),
    ("swashOut", _("swashOut")),
    ("swing", _("swing")),
    ("tada", _("tada")),
    ("typing_and_erasing", _("typing_and_erasing")),
    ("typing", _("typing")),
    ("tinLeftIn", _("tinLeftIn")),
    ("tinRightIn", _("tinRightIn")),
    ("tinUpIn", _("tinUpIn")),
    ("tinDownIn", _("tinDownIn")),
    ("tinLeftOut", _("tinLeftOut")),
    ("tinRightOut", _("tinRightOut")),
    ("tinUpOut", _("tinUpOut")),
    ("tinDownOut", _("tinDownOut")),
    ("tossing", _("tossing")),
    ("twisterInUp", _("twisterInUp")),
    ("twisterInDown", _("twisterInDown")),
    ("vanishIn", _("vanishIn")),
    ("vanishOut", _("vanishOut")),
    ("wobble", _("wobble")),
    ("zoomIn", _("zoomIn")),
    ("zoomOut", _("zoomOut")),
    ("zoomOutSlowly", _("zoomOutSlowly")),
    ("zoomInRight", _("zoomInRight")),
    ("zoomInUp", _("zoomInUp")),
    ("zoomInDown", _("zoomInDown")),
    ("zoomOutLeft", _("zoomOutLeft")),
    ("zoomOutRight", _("zoomOutRight")),
    ("zoomOutUp", _("zoomOutUp")),
    ("zoomOutDown", _("zoomOutDown")),
    ("planeFW", _("planeFW")),
    ("planeFWFixed", _("planeFWFixed")),
    ("planeBW", _("planeBW")),
    ("planeBWFixed", _("planeBWFixed")),
    ("planeBW2", _("planeBW2")),
    ("magnifyingBounce", _("magnifyingBounce")),
    ("stamp", _("stamp")),
    ("returnsToZero", _("returnsToZero")),
    ("pulse", _("pulse")),
    ("pullLeftBoxRegularRZ", _("pullLeftBoxRegularRZ")),
    ("lbg1_bgk", _("lbg1_bgk")),
    ("slideBox_RightFromLeft", _("slideBox_RightFromLeft")),
    ("slideBox_LeftFromRight", _("slideBox_LeftFromRight")),
    ("slideBox_RightFromLeft_min", _("slideBox_RightFromLeft_min")),
    ("slideBox_LeftFromRight_min", _("slideBox_LeftFromRight_min")),
    ("slideBox_TopFromBottom", _("slideBox_TopFromBottom")),
    ("slideBox_BottomFromTop", _("slideBox_BottomFromTop")),
    ("slideBox_TopFromBottom_min", _("slideBox_TopFromBottom_min")),
    ("slideBox_BottomFromTop_min", _("slideBox_BottomFromTop_min")),
    ("slideBox_TopFromBottom_full", _("slideBox_TopFromBottom_full")),
    ("slideBox_TopFromBottom_full_min", _("slideBox_TopFromBottom_full_min")),
]


HORIZONTAL_POSITION_CHOICES = [
    ('left', _('Left')),
    ('center', _('Center')),
    ('right', _('Right')),
]


VERTICAL_POSITION_CHOICES = [
    ('top', _('Top')),
    ('center', _('Center')),
    ('bottom', _('Bottom')),
]


# Add additional choices through the ``settings.py``.
def get_templates():
    return [
        ('default', _('Default')),
    ] + getattr(
        settings,
        'DJANGOCMS_SLIDESHOW_TEMPLATES',
        [],
    )


@python_2_unicode_compatible
class Slideshow(CMSPlugin):
    template = models.CharField(
        verbose_name=_('Template'),
        choices=get_templates(),
        default=get_templates()[0][0],
        max_length=255,
    )
    # attributes = AttributesField(
    #     verbose_name=_('Attributes'),
    #     blank=True,
    # )

    skin = models.CharField(
        verbose_name=_('Skin'),
        default="opportune",
        choices=[
            ("opportune", _("Opportune")),
            ("majestic", _("Majestic")),
            ("generous", _("Generous")),
        ],
        max_length=30,
    )
    responsive = models.BooleanField(
        default=True,
    )
    responsive_relative_to_browser = models.BooleanField(
        default=True,
        help_text=_(
            "true - the banner will be responisve relatively to browser "
            "dimensions. "
            "false - the banner will be responisve relatively to parent div."
        )
    )
    width = models.PositiveSmallIntegerField(
        default=918,
        help_text=_(
            "Banner width. If responsive=false this is the exact banner "
            "width. If responsive=true this is also used to determine resize "
            "proportions"
        ),
    )
    height = models.PositiveSmallIntegerField(
        default=382,
        help_text=_(
            "Banner height. If responsive=false this is the exact banner "
            "height. If responsive=true this is also used to determine resize "
            "proportions"
        ),
    )
    width_100_proc = models.BooleanField(
        default=False,
        help_text=_(
            "true - the banner width will be 100%"
            "false - the banner width will be what you've set for 'width' "
            "parameter"
        )
    )
    height_100_proc = models.BooleanField(
        default=False,
        help_text=_(
            "true - the banner height will be 100%"
            "false - the banner height will be what you've set for 'width' "
            "parameter"
        )
    )
    fade_slides = models.BooleanField(
        default=True,
        help_text=_(
            "true - the transition between the slides will be 'Fade'"
            "false - the transition between the slides will be 'Slide'"
        )
    )
    auto_play = models.PositiveSmallIntegerField(
        default=10,
        help_text=_(
            "You can define the time (in seconds) until the next slide will "
            "play. If you set it 0 the banner will not autoplay. "
            "If needed, for each slide you can set particular auto-play time"
        ),
    )
    loop = models.BooleanField(
        default=True,
        help_text=_(
            "If set the banner will loop when reaches the end, otherwise "
            "it will stop."
        )
    )
    # target = models.CharField(
    #     verbose_name=_('Link target'),
    #     default="_blank",
    #     choices=[
    #         ("_blank", _("Blank")),
    #         ("_self", _("Self")),
    #     ],
    #     help_text=_(
    #        "The target of the link associated to the primary image."
    #     )
    # )
    # enableTouchScreen

    myloader_time = models.PositiveSmallIntegerField(
        default=2,
        help_text=_(
            "The delay time (in seconds) to postpone the slider start. "
            "In this time, a rotating cicle will appear in the middle of the "
            "slider to signal that the slider is in loading status. "
            "NOTE: to gain time when you are testing your slider, because "
            "the slider is already in cache, set this time to 0."
        ),
    )
    pause_on_mouse_over = models.BooleanField(
        default=False,
        help_text=_(
            "Sets auto_play pause on mouse over."
        )
    )
    show_pause_button = models.BooleanField(
        default=True,
        help_text=_(
            "Shows pause button."
        )
    )
    set_as_bg = models.BooleanField(
        default=False,
        help_text=_(
            "Set if you intend to use the plugin as full screen background."
        )
    )
    # absUrl - the url to the skins folder.
    slide_duration = models.DecimalField(
        default=0.8,
        max_digits=3,
        decimal_places=2,
        help_text=_(
            "Time to make the transition/animation from one slide to another."
        ),
    )
    SLIDE_EASING_CHOICES = [
        ("linear", _("linear")),
        ("swing ", _("swing ")),
        ("easeInQuad", _("easeInQuad")),
        ("easeOutQuad", _("easeOutQuad")),
        ("easeInOutQuad", _("easeInOutQuad")),
        ("easeInCubic", _("easeInCubic")),
        ("easeOutCubic", _("easeOutCubic")),
        ("easeInOutCubic", _("easeInOutCubic")),
        ("easeInQuart", _("easeInQuart")),
        ("easeOutQuart", _("easeOutQuart")),
        ("easeInOutQuart", _("easeInOutQuart")),
        ("easeInSine", _("easeInSine")),
        ("easeOutSine", _("easeOutSine")),
        ("easeInOutSine", _("easeInOutSine")),
        ("easeInExpo", _("easeInExpo")),
        ("easeOutExpo", _("easeOutExpo")),
        ("easeInOutExpo", _("easeInOutExpo")),
        ("easeInQuint", _("easeInQuint")),
        ("easeOutQuint", _("easeOutQuint")),
        ("easeInOutQuint", _("easeInOutQuint")),
        ("easeInCirc", _("easeInCirc")),
        ("easeOutCirc", _("easeOutCirc")),
        ("easeInOutCirc", _("easeInOutCirc")),
        ("easeInElastic", _("easeInElastic")),
        ("easeOutElastic", _("easeOutElastic")),
        ("easeInOutElastic", _("easeInOutElastic")),
        ("easeInBack", _("easeInBack")),
        ("easeOutBack", _("easeOutBack")),
        ("easeInOutBack", _("easeInOutBack")),
        ("easeInBounce", _("easeInBounce")),
        ("easeOutBounce", _("easeOutBounce")),
        ("easeInOutBounce", _("easeInOutBounce")),
    ]
    scroll_slide_easing = models.CharField(
        max_length=30,
        default="easeOutQuad",
        choices=SLIDE_EASING_CHOICES,
        help_text=_("The animation (from one slide to another) easing."),
    )
    layer_default_easing = models.CharField(
        max_length=36,
        default="swing",
        choices=EASING_CHOICES,
        help_text=_("The defaut easing for layer animation."),
    )
    # thumbsWrapperMarginTop
    # - the vertical position of the thumbs area
    # - default: 0
    # hideControlsUnder
    # - the resolution under which the 'next' & 'previous' navigation buttons
    #   will not be visible. If you set 0 this parameter will be ignored and
    #   the navigation buttons will be always visible no matter the screen
    #   resolution.
    # - default: 768

    # Ken Burns Effect Settings
    horizontal_position = models.CharField(
        verbose_name=_('Horizontal position'),
        default="center",
        choices=HORIZONTAL_POSITION_CHOICES,
        max_length=30,
        help_text=_(
            "Using this parameter the script will determine both the photo "
            "initial horizontal position and KenBurns effect direction."
        ),
    )
    vertical_position = models.CharField(
        verbose_name=_('Vertical position'),
        default="center",
        choices=HORIZONTAL_POSITION_CHOICES,
        max_length=30,
        help_text=_(
            "Using this parameter the script will determine both the photo "
            "initial vertical position and KenBurns effect direction."
        ),
    )
    initial_zoom = models.DecimalField(
        default=1,
        max_digits=3,
        decimal_places=2,
        validators=[MinValueValidator(0.6), MaxValueValidator(2.0)],
        help_text=_(
            "Initial photo zoom, recommended values between 0.6 and 2."
        ),
    )
    final_zoom = models.DecimalField(
        default=0.8,
        max_digits=3,
        decimal_places=2,
        validators=[MinValueValidator(0.6), MaxValueValidator(2.0)],
        help_text=_(
            "Final photo zoom, recommended values between 0.6 and 2."
        ),
    )
    duration = models.PositiveSmallIntegerField(
        default=20,
        help_text=_("KenBurns effect duration in seconds"),
    )
    # durationIEfix - check help
    zoom_easing = models.CharField(
        verbose_name=_('Zoom easing'),
        default="ease",
        choices=EASING_CHOICES,
        max_length=30,
        help_text=_("The KenBurns effect easing."),
    )

    # Thumbs Settings - available only for majestic and generous skins
    number_of_thumbs_per_screen = models.SmallIntegerField(
        default=0,
        help_text=_(
            "The number of thumbs per screen. If you set it to 0, it will be "
            "calculated automatically. You can set a fixed number, for "
            "example 4"
        ),
    )
    thumbs_on_margin_top = models.SmallIntegerField(
        default=0,
        help_text=_(
            "For the thumbs that are on, there is an arrow pointing the "
            "active thumb. Using this parameter you can change the vertical "
            "position of this arrow"
        ),
    )

    # Circle Timer Settings
    show_circle_timer = models.BooleanField(
        default=True,
        help_text=_("Shows the circle timer."),
    )
    circle_radius = models.PositiveSmallIntegerField(
        default=13,
        help_text=_("Circle radius."),
    )
    circle_linewidth = models.PositiveSmallIntegerField(
        default=2,
        help_text=_("Circle line width."),
    )
    circle_color = models.CharField(
        default="#FFFFFF",
        max_length=10,
        help_text=_("Circle color."),
    )
    circle_alpha = models.SmallIntegerField(
        default=100,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Circle alpha, in percents."),
    )
    behind_circle_color = models.CharField(
        default="#FFFFFF",
        max_length=10,
        help_text=_("Behind circle color."),
    )
    behind_circle_alpha = models.SmallIntegerField(
        default=20,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Behind circle alpha, in percents."),
    )

    # Default Values For Layers Exit Move
    default_exit_left = models.SmallIntegerField(
        verbose_name=_('Default exit left'),
        default=0,
        help_text=_(
            "The layer exit left postion (in pixels). "
        ),
    )
    default_exit_top = models.SmallIntegerField(
        verbose_name=_('Default exit top'),
        default=0,
        help_text=_(
            "The layer exit top postion (in pixels)."
        ),
    )
    default_exit_duration = models.SmallIntegerField(
        verbose_name=_('Default exit duration'),
        default=0,
        help_text=_(
            "The exit animation duration (in seconds)."
        ),
    )
    default_exit_fade = models.DecimalField(
        verbose_name=_('Default exit fade'),
        default=1,
        max_digits=3,
        decimal_places=2,
        validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
        help_text=_(
            "The default opacity value, for layer exit (values between 0-1)."
        ),
    )
    default_exit_delay = models.DecimalField(
        verbose_name=_('Default exit delay'),
        default=0,
        max_digits=3,
        decimal_places=2,
        help_text=_(
            "Exit delay time (in seconds)."
        ),
    )
    default_exit_easing = models.CharField(
        verbose_name=_('Default exit easing'),
        default="swing",
        choices=EASING_CHOICES,
        max_length=30,
        help_text=_(
            "The easing for layer animation."
        ),
    )
    default_exit_off = models.CharField(
        default="false",
        max_length=10,
        choices=[
            ("true", _("true")),
            ("false", _("false")),
        ],
        help_text=_(
            "Possible values: "
            "true - the layer will not exit"
            "false - the layer will exit according to exit left, top, delay, "
            "duration and easing value"
        ),
    )

    # Controllers Setting
    show_all_controllers = models.BooleanField(
        default=True,
        help_text=_(
            "All controllers will appear (next, previous, bottom navigation)."
        )
    )
    show_nav_arrows = models.BooleanField(
        default=True,
        help_text=_(
            "Next, previous buttons will appear."
        )
    )
    show_on_init_nav_arrows = models.BooleanField(
        default=True,
        help_text=_(
            "Next, previous buttons will appear on first banner init."
        )
    )
    auto_hide_nav_arrows = models.BooleanField(
        default=True,
        help_text=_(
            "Next, previous buttons will hide when mouse out."
        )
    )
    show_bottom_nav = models.BooleanField(
        default=True,
        help_text=_(
            "Bottom navigation buttons will appear."
        )
    )
    show_on_init_bottom_nav = models.BooleanField(
        default=True,
        help_text=_(
            "Bottom navigation buttons will appear on first banner init."
        )
    )
    auto_hide_bottom_nav = models.BooleanField(
        default=False,
        help_text=_(
            "Bottom navigations buttons will hide when mouse out."
        )
    )
    show_preview_thumbs = models.BooleanField(
        default=True,
        help_text=_(
            "A preview image will appear on hovering each bottom button."
        )
    )

    # Add an app namespace to related_name to avoid field name clashes
    # with any other plugins that have a field with the same name as the
    # lowercase of the class name of this model.
    # https://github.com/divio/django-cms/issues/5030
    cmsplugin_ptr = models.OneToOneField(
        CMSPlugin,
        related_name='%(app_label)s_%(class)s',
        parent_link=True,
    )

    def __str__(self):
        return "Slideshow %s" % str(self.pk)


@python_2_unicode_compatible
class Slide(CMSPlugin):
    image = FilerImageField(
        verbose_name=_('Image'),
        related_name='+',
    )

    horizontal_position = models.CharField(
        verbose_name=_('Horizontal position'),
        blank=True,
        null=True,
        choices=HORIZONTAL_POSITION_CHOICES,
        max_length=30,
        help_text=_(
            "Using this parameter the script will determine both the photo "
            "initial horizontal position and KenBurns effect direction."
        ),
    )

    vertical_position = models.CharField(
        verbose_name=_('Vertical position'),
        blank=True,
        null=True,
        choices=VERTICAL_POSITION_CHOICES,
        max_length=30,
        help_text=_(
            "Using this parameter the script will determine both the photo "
            "initial vertical position and KenBurns effect direction."
        ),
    )
    initial_zoom = models.DecimalField(
        blank=True,
        null=True,
        max_digits=3,
        decimal_places=2,
        validators=[MinValueValidator(0.6), MaxValueValidator(2.0)],
        help_text=_(
            "Initial photo zoom, recommended values between 0.6 and 2."
        ),
    )
    final_zoom = models.DecimalField(
        blank=True,
        null=True,
        max_digits=3,
        decimal_places=2,
        validators=[MinValueValidator(0.6), MaxValueValidator(2.0)],
        help_text=_(
            "Final photo zoom, recommended values between 0.6 and 2."
        ),
    )
    duration = models.PositiveSmallIntegerField(
        blank=True,
        null=True,
        help_text=_("KenBurns effect duration in seconds"),
    )
    zoom_easing = models.CharField(
        verbose_name=_('Zoom easing'),
        blank=True,
        null=True,
        choices=EASING_CHOICES,
        max_length=30,
        help_text=_("The KenBurns effect easing."),
    )
    auto_play = models.PositiveSmallIntegerField(
        verbose_name=_('Slide duration (auto_play)'),
        blank=True,
        null=True,
        help_text=_(
            "You can define for each slide the time (in seconds) until the "
            "next slide will play. If you set it 0 the slider will not "
            "autoplay."
        ),
    )

    # Check jquery-slider-zoom-inout-effect-fully-responsive help.html file
    # link
    # target

    # Add an app namespace to related_name to avoid field name clashes
    # with any other plugins that have a field with the same name as the
    # lowercase of the class name of this model.
    # https://github.com/divio/django-cms/issues/5030
    cmsplugin_ptr = models.OneToOneField(
        CMSPlugin,
        related_name='%(app_label)s_%(class)s',
        parent_link=True,
    )

    def __str__(self):
        return "Slide %s (%s)" % (str(self.pk), self.image.label)


# @python_2_unicode_compatible
# class Layers(CMSPlugin):
#     pass


@python_2_unicode_compatible
class Layer(CMSPlugin):

    initial_left = models.SmallIntegerField(
        verbose_name=_('Initial left'),
        default=0,
        help_text=_("The layer initial left postion (in pixels)."),
    )
    initial_top = models.SmallIntegerField(
        verbose_name=_('Initial top'),
        default=0,
        help_text=_("The layer initial top postion (in pixels)."),
    )
    final_left = models.SmallIntegerField(
        verbose_name=_('Final left'),
        default=0,
        help_text=_("The layer final left postion (in pixels)."),
    )
    final_top = models.SmallIntegerField(
        verbose_name=_('Final top'),
        default=0,
        help_text=_("The layer final top postion (in pixels)."),
    )
    duration = models.SmallIntegerField(
        verbose_name=_('Duration'),
        default=3,
        help_text=_("The animation duration (in seconds)."),
    )

    fade_start = models.SmallIntegerField(
        verbose_name=_('Fade start'),
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Initial fade value. Values from 0 to 100."),
    )
    delay = models.DecimalField(
        verbose_name=_('Delay'),
        default=1.0,
        max_digits=3,
        decimal_places=2,
        help_text=_("Delay time (in seconds)."),
    )
    easing = models.CharField(
        verbose_name=_('Easing'),
        blank=True,
        null=True,
        choices=EASING_CHOICES,
        max_length=30,
        help_text=_(
            "The easing for layer animation. If this parameter is not added, "
            "the value of slideshow parameter default_easing, will be used."
        ),
    )
    css3_animation = models.CharField(
        verbose_name=_('CSS3 animation'),
        blank=True,
        null=True,
        choices=CSS3_ANIMATION_CHOICES,
        max_length=36,
        help_text=_(
            "The initial CSS3 prebuilt animation for layer."
        ),
    )

    # Intermediate
    intermediate_left = models.SmallIntegerField(
        verbose_name=_('Intermediate left'),
        blank=True,
        null=True,
        help_text=_("The layer intermediate left postion (in pixels)."),
    )
    intermediate_top = models.SmallIntegerField(
        verbose_name=_('Intermediate top'),
        blank=True,
        null=True,
        help_text=_("The layer intermediate top postion (in pixels)."),
    )
    intermediate_duration = models.SmallIntegerField(
        verbose_name=_('Intermediate duration'),
        blank=True,
        null=True,
        help_text=_("The intermediate duration (in seconds)."),
    )
    intermediate_delay = models.DecimalField(
        verbose_name=_('Intermediate delay'),
        blank=True,
        null=True,
        max_digits=3,
        decimal_places=2,
        help_text=_("Delay time (in seconds)."),
    )
    intermediate_easing = models.CharField(
        verbose_name=_('Intermediate Easing'),
        blank=True,
        null=True,
        choices=EASING_CHOICES,
        max_length=30,
        help_text=_(
            "The easing for layer animation. If this parameter is not added, "
            "the value of slideshow parameter 'default_easing, will be used."
        ),
    )
    intermediate_css3_animation = models.CharField(
        verbose_name=_('Intermediate CSS3 animation'),
        blank=True,
        null=True,
        choices=CSS3_ANIMATION_CHOICES,
        max_length=36,
        help_text=_(
            "The initial CSS3 prebuilt animation for layer."
        ),
    )

    # Exit
    exit_left = models.SmallIntegerField(
        verbose_name=_('Exit left'),
        blank=True,
        null=True,
        help_text=_(
            "The layer exit left postion (in pixels). "
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_top = models.SmallIntegerField(
        verbose_name=_('Exit top'),
        blank=True,
        null=True,
        help_text=_(
            "The layer exit top postion (in pixels)."
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_duration = models.SmallIntegerField(
        verbose_name=_('Exit duration'),
        blank=True,
        null=True,
        help_text=_(
            "The exit animation duration (in seconds)."
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_delay = models.DecimalField(
        verbose_name=_('Exit delay'),
        blank=True,
        null=True,
        max_digits=3,
        decimal_places=2,
        help_text=_(
            "Exit delay time (in seconds)."
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_fade = models.SmallIntegerField(
        verbose_name=_('Exit fade'),
        blank=True,
        null=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_(
            "Exit fade value. Values from 0 to 100."
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_easing = models.CharField(
        verbose_name=_('Exit easing'),
        blank=True,
        null=True,
        choices=EASING_CHOICES,
        max_length=30,
        help_text=_(
            "The easing for layer animation."
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_off = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        choices=[
            ("true", _("true")),
            ("false", _("false")),
        ],
        help_text=_(
            "Possible values: "
            "true - the layer will not exit"
            "false - the layer will exit according to exit left, top, delay, "
            "duration and easing value"
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )
    exit_css3_animation = models.CharField(
        verbose_name=_('Exit CSS3 animation'),
        blank=True,
        null=True,
        choices=CSS3_ANIMATION_CHOICES,
        max_length=36,
        help_text=_(
            "The exit CSS3 prebuilt animation for layer."
            "If this parameter is not added, the value slideshow parameter is "
            "used."
        ),
    )

    # Add an app namespace to related_name to avoid field name clashes
    # with any other plugins that have a field with the same name as the
    # lowercase of the class name of this model.
    # https://github.com/divio/django-cms/issues/5030
    cmsplugin_ptr = models.OneToOneField(
        CMSPlugin,
        related_name='%(app_label)s_%(class)s',
        parent_link=True,
    )

    def __str__(self):
        return "Layer %s" % str(self.pk)
