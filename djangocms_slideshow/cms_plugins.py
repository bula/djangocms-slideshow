# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from . import models


class SlideshowPlugin(CMSPluginBase):
    model = models.Slideshow
    name = _('Slideshow')
    text_enabled = False
    allow_children = True
    child_classes = ['SlidePlugin', ]

    fieldsets = [
        (None, {
            'fields': (
                'template',
                'auto_play',
                'slide_duration',
                'skin',
                'responsive',
                'responsive_relative_to_browser',
                'width',
                'height',
                'width_100_proc',
                'height_100_proc',
                'fade_slides',
                'loop',
                'myloader_time',
                'pause_on_mouse_over',
                'show_pause_button',
                'set_as_bg',
                'scroll_slide_easing',
                'layer_default_easing',
            )
        }),
        (_('Ken Burns Effect Settings'), {
            'classes': ('collapse',),
            'fields': (
                'horizontal_position',
                'vertical_position',
                'initial_zoom',
                'final_zoom',
                'duration',
                'zoom_easing',
            )
        }),
        (_('Thumbs Settings - only for majestic and generous skins'), {
            'classes': ('collapse',),
            'fields': (
                'number_of_thumbs_per_screen',
                'thumbs_on_margin_top',
            )
        }),
        (_('Circle Timer Settings'), {
            'classes': ('collapse',),
            'fields': (
                'show_circle_timer',
                'circle_radius',
                'circle_linewidth',
                'circle_color',
                'circle_alpha',
                'behind_circle_color',
                'behind_circle_alpha',
            )
        }),
        (_('Default Values For Layers Exit Move'), {
            'classes': ('collapse',),
            'fields': (
                'default_exit_left',
                'default_exit_top',
                'default_exit_duration',
                'default_exit_fade',
                'default_exit_delay',
                'default_exit_off',
            )
        }),
        (_('Controllers Setting'), {
            'classes': ('collapse',),
            'fields': (
                'show_all_controllers',
                'show_nav_arrows',
                'show_on_init_nav_arrows',
                'auto_hide_nav_arrows',
                'show_bottom_nav',
                'show_on_init_bottom_nav',
                'auto_hide_bottom_nav',
                'show_preview_thumbs',
            )
        }),
    ]

    def render(self, context, instance, placeholder):
        context = super(SlideshowPlugin, self)\
            .render(context, instance, placeholder)
        context['slideshow_template'] = instance.template
        return context

    def get_render_template(self, context, instance, placeholder):
        return 'djangocms_slideshow/{}/slideshow.html'\
            .format(instance.template)


class SlidePlugin(CMSPluginBase):
    model = models.Slide
    name = _('Slide')
    module = _('Slideshow')
    text_enabled = False
    require_parent = True
    parent_classes = ['SlideshowPlugin', ]
    allow_children = True
    child_classes = ['FilerImagePlugin', 'VideoPlayerPlugin', 'LayersPlugin', ]

    fieldsets = [
        (None, {
            'fields': (
                'image',
            )
        }),
        (_("Animation settings"), {
            'fields': (
                'auto_play',
                'horizontal_position',
                'vertical_position',
                'initial_zoom',
                'final_zoom',
                'duration',
                'zoom_easing',
            ),
            'description': _("When setting is unset slide uses default value "
                             "from slideshow settings.")
        }),
    ]

    def get_render_template(self, context, instance, placeholder):
        if instance.child_plugin_instances:
            for plugin in instance.child_plugin_instances:
                if plugin.plugin_type == "LayersPlugin":
                    context.update({'layers_id': plugin.id})
                if plugin.plugin_type == "VideoPlayerPlugin":
                    context.update({'video': True})
        return 'djangocms_slideshow/{}/slide.html'\
            .format(context['slideshow_template'])


class LayersPlugin(CMSPluginBase):
    name = _('Layers')
    module = _('Slideshow')
    require_parent = True
    parent_classes = ['SlidePlugin', ]
    allow_children = True
    child_classes = ['LayerPlugin', ]

    def get_render_template(self, context, instance, placeholder):
        return 'djangocms_slideshow/{}/layers.html'\
            .format(context['slideshow_template'])


class LayerPlugin(CMSPluginBase):
    model = models.Layer
    name = _('Layer')
    module = _('Slideshow')
    require_parent = True
    parent_classes = ['LayersPlugin', ]
    allow_children = True
    child_classes = ['FilerImagePlugin', 'TextPlugin', ]

    fieldsets = [
        (None, {
            'fields': (
                'initial_left',
                'initial_top',
                'final_left',
                'final_top',
                'duration',
                'fade_start',
                'delay',
                'easing',
                'css3_animation',
            )
        }),
        (_('Intermediate'), {
            'classes': ('collapse',),
            'fields': (
                'intermediate_left',
                'intermediate_top',
                'intermediate_duration',
                'intermediate_delay',
                'intermediate_easing',
                'intermediate_css3_animation',
            )
        }),
        (_('Exit'), {
            'classes': ('collapse',),
            'fields': (
                'exit_left',
                'exit_top',
                'exit_duration',
                'exit_delay',
                'exit_fade',
                'exit_easing',
                'exit_off',
                'exit_css3_animation',
            )
        }),
    ]

    def get_render_template(self, context, instance, placeholder):
        return 'djangocms_slideshow/{}/layer.html'\
            .format(context['slideshow_template'])


plugin_pool.register_plugin(SlideshowPlugin)
plugin_pool.register_plugin(SlidePlugin)
plugin_pool.register_plugin(LayersPlugin)
plugin_pool.register_plugin(LayerPlugin)
